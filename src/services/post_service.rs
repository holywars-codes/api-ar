use std::sync::Arc;

use diesel::prelude::*;
use diesel::r2d2::{ConnectionManager, Pool};
use diesel::{delete, insert_into, QueryDsl};
use diesel_async::AsyncPgConnection;

use crate::models::pageable::Pageable;
use crate::models::post_model::{Post, PostForm};
use crate::schema::posts::dsl::posts;
use crate::schema::posts::{id, published};

// #[derive(Clone)]
pub struct PostService {
    pool: Arc<Pool<ConnectionManager<PgConnection>>>,
    a_pool: Arc<diesel_async::pooled_connection::bb8::Pool<AsyncPgConnection>>,
}

impl PostService {
    pub fn new(
        pool: Arc<Pool<ConnectionManager<PgConnection>>>,
        a_pool: Arc<diesel_async::pooled_connection::bb8::Pool<AsyncPgConnection>>,
    ) -> PostService {
        PostService { pool, a_pool }
    }

    pub fn get_posts(
        &self,
        pool: &Pool<ConnectionManager<PgConnection>>,
        pageable: &Pageable,
    ) -> Vec<Post> {
        // let conn = &mut establish_connection();
        // let pool = get_connection_pool();
        let conn = &mut pool.get().unwrap();
        posts
            // .filter(published.eq(true))
            .limit(pageable.limit)
            .offset(pageable.offset)
            .select(Post::as_select())
            .load(conn)
            .unwrap()
    }

    pub fn get_post_by_id(
        &self,
        pool: &Pool<ConnectionManager<PgConnection>>,
        post_id: i32,
    ) -> Post {
        let conn = &mut pool.get().unwrap();
        // let conn = &mut self.pool.get().unwrap();
        posts
            .filter(id.eq(post_id))
            .select(Post::as_select())
            .get_result(conn)
            .unwrap()
    }

    pub async fn get_post_async_by_id(&self, post_id: i32) -> Post {
        let conn = &mut self.a_pool.get().await.unwrap();
        let s = posts.filter(id.eq(post_id)).select(Post::as_select());
        diesel_async::RunQueryDsl::get_result(s, conn)
            .await
            .unwrap()
    }

    pub fn create_post(&self, post_form: &PostForm) -> Post {
        let conn = &mut self.pool.get().unwrap();
        insert_into(posts)
            .values(post_form)
            .get_result(conn)
            .unwrap()
    }

    pub fn remove_post(&self, post_id: i32) -> QueryResult<usize> {
        let conn = &mut self.pool.get().unwrap();
        delete(posts.filter(id.eq(post_id))).execute(conn)
    }
}
