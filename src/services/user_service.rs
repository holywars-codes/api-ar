use std::io::stderr;
use std::sync::Mutex;

use crate::models::user_model::UserModel;

pub struct UserService {
    users: Mutex<Vec<UserModel>>,
}

impl UserService {
    pub fn new(option_users: Option<Vec<UserModel>>) -> UserService {
        UserService {
            users: Mutex::new(match option_users {
                None => Vec::new(),
                Some(u) => u,
            }),
        }
    }
    pub fn get_list(&self) -> &Mutex<Vec<UserModel>> {
        &self.users
    }

    pub fn add_user(&self, first_name: String, last_name: String) {
        let user = UserModel::new(first_name, last_name);
        // let mut users = &mut self.users;
        let mut u = self.users.lock().unwrap();
        u.push(user);
        // users.push(user);
        ()
    }

    pub fn get_one(&self, first_name: &String, last_name: &String) -> UserModel {
        self.users
            .lock()
            .unwrap()
            .iter()
            .find(|u| u.first_name.eq(first_name) && u.last_name.eq(last_name))
            .unwrap()
            .clone()
    }

    pub fn del_one(&mut self, first_name: &String, last_name: &String) {
        self.users
            .lock()
            .unwrap()
            .retain(|u| u.first_name.eq(first_name) && u.last_name.eq(last_name));
    }
}
