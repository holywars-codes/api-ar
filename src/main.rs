use std::sync::Arc;

use crate::connection::{get_async_connection_pool, get_connection_pool};
use crate::handlers::post_handler::{
    create_post, get_post_async_by_id, get_post_by_id, get_posts, remove_post,
};
use actix_web::web::Data;
use actix_web::{App, HttpServer};

use crate::handlers::user_handler::{create_user, get_users, search_user};
use crate::models::user_model::UserModel;
use crate::services::post_service::PostService;
use crate::services::user_service::UserService;
use crate::services::Services;

mod handlers;
mod models;

mod connection;
mod schema;
mod services;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // std::env::set_var("RUST_LOG", "debug");
    // env_logger::init();

    let user_service = Arc::new(UserService::new(Some(vec![UserModel {
        first_name: "ivan".to_string(),
        last_name: "ivanov".to_string(),
    }])));

    let pool = Arc::new(get_connection_pool());

    let pool_factory = get_async_connection_pool().await;

    let a_pool = Arc::new(pool_factory);

    let post_service = Arc::new(PostService::new(pool.clone(), a_pool.clone()));

    let services = Services::new(user_service, post_service, pool);

    HttpServer::new(move || {
        App::new()
            .app_data(Data::new(services.clone()))
            .service(get_users)
            .service(search_user)
            .service(create_user)
            .service(get_posts)
            .service(get_post_by_id)
            .service(create_post)
            .service(remove_post)
            .service(get_post_async_by_id)
    })
    .workers(10)
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
