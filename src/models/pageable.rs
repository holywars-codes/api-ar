use serde::Deserialize;

#[derive(Deserialize)]
pub struct Pageable {
    pub limit: i64,
    pub offset: i64,
}
