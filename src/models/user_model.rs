use actix_web::body::BoxBody;
use actix_web::http::header::ContentType;
use actix_web::{HttpRequest, HttpResponse, Responder};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct UserModel {
    pub first_name: String,
    pub last_name: String,
}

impl UserModel {
    pub fn new(first_name: String, last_name: String) -> UserModel {
        UserModel {
            first_name,
            last_name,
        }
    }
}

// impl Responder for UserModel {
//     type Body = BoxBody;
//
//     fn respond_to(self, req: &HttpRequest) -> HttpResponse<Self::Body> {
//         let body = serde_json::to_string(&self).unwrap();
//         HttpResponse::Ok()
//             .content_type(ContentType::json())
//             .body(body)
//     }
// }

// impl Responder for Vec<UserModel> {
//     type Body = BoxBody;
//
//     fn respond_to(self, req: &HttpRequest) -> HttpResponse<Self::Body> {
//         let body = serde_json::to_string(&self).unwrap();
//         HttpResponse::Ok().content_type(ContentType::json())
//             .body(body)
//     }
// }
