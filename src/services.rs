use diesel::r2d2::{ConnectionManager, Pool};
use diesel::PgConnection;
use std::sync::Arc;

use crate::services::post_service::PostService;
use crate::services::user_service::UserService;

pub mod post_service;
pub mod user_service;

#[derive(Clone)]
pub struct Services {
    user_service: Arc<UserService>,
    post_service: Arc<PostService>,
    pool: Arc<Pool<ConnectionManager<PgConnection>>>,
}

impl Services {
    pub fn new(
        user_service: Arc<UserService>,
        post_service: Arc<PostService>,
        pool: Arc<Pool<ConnectionManager<PgConnection>>>,
    ) -> Services {
        Services {
            user_service,
            post_service,
            pool,
        }
    }
    pub fn user_service(&self) -> &UserService {
        &self.user_service
    }
    pub fn post_service(&self) -> &PostService {
        &self.post_service
    }
    pub fn pool(&self) -> &Pool<ConnectionManager<PgConnection>> {
        &self.pool
    }
}
