use crate::models::pageable::Pageable;
use actix_web::http::StatusCode;
use actix_web::web::{Data, Json, Path, Query};
use actix_web::{delete, get, post, HttpResponse};
use serde::Deserialize;

use crate::models::post_model::PostForm;
use crate::services::Services;

#[get("/posts")]
pub async fn get_posts(services: Data<Services>, query: Query<Pageable>) -> HttpResponse {
    HttpResponse::Ok().json(services.post_service().get_posts(services.pool(), &query))
}

#[derive(Deserialize)]
pub struct UserId {
    pub id: i32,
}
#[get("/post/{id}")]
pub async fn get_post_by_id(services: Data<Services>, info: Path<UserId>) -> HttpResponse {
    HttpResponse::Ok().json(
        services
            .post_service()
            .get_post_by_id(services.pool(), info.id),
    )
}

#[get("/a/post/{id}")]
pub async fn get_post_async_by_id(services: Data<Services>, info: Path<UserId>) -> HttpResponse {
    let post = services.post_service().get_post_async_by_id(info.id).await;
    HttpResponse::Ok().json(post)
}
#[post("/post")]
pub async fn create_post(services: Data<Services>, body: Json<PostForm>) -> HttpResponse {
    HttpResponse::Ok()
        .status(StatusCode::CREATED)
        .json(services.post_service().create_post(&body))
}

#[delete("/post/{id}")]
pub async fn remove_post(services: Data<Services>, info: Path<UserId>) -> HttpResponse {
    match services.post_service().remove_post(info.id) {
        Ok(_) => HttpResponse::Ok().finish(),
        Err(e) => HttpResponse::InternalServerError().json(e.to_string()),
    }
}
