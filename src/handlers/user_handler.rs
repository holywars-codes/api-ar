use std::ops::Deref;

use actix_web::web::{Data, Json, Query};
use actix_web::{get, post, HttpResponse};
use serde::Deserialize;

use crate::models::user_model::UserModel;
use crate::services::Services;

#[derive(Deserialize)]
pub struct UserQuery {
    pub first_name: String,
    pub last_name: String,
}

#[get("/users")]
pub async fn get_users(services: Data<Services>) -> HttpResponse {
    HttpResponse::Ok().json(services.user_service().get_list().lock().unwrap().deref())
}

#[get("/user")]
pub async fn search_user(services: Data<Services>, query: Query<UserQuery>) -> HttpResponse {
    HttpResponse::Ok().json(
        services
            .user_service()
            .get_one(&query.first_name, &query.last_name),
    )
}

#[post("/user/add")]
pub async fn create_user(services: Data<Services>, body: Json<UserModel>) -> HttpResponse {
    let first_name = &body.first_name;
    let last_name = &body.last_name;

    services
        .user_service()
        .add_user(first_name.clone(), last_name.clone());
    //
    HttpResponse::Ok().finish()
}
